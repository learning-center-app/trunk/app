import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import AsyncStorage from '@react-native-community/async-storage'

// More info here:  https://shift.infinite.red/shipping-persistant-reducers-7341691232b1
const REDUX_PERSIST = {
  active: true,
  //Changer cette version pour forcer le ryhdrate du store lors de l'update de l'app
  //Voir lien ignite ci dessus pour plus d'info. Utiliser les mêmes versions que dans package.json si possible
  //Utilisé par App/Services/Rehydration.js
  reducerVersion: '1.2.1', 
  storeConfig: {
    key: 'primary',
    storage: AsyncStorage,
    // Reducer keys that you do NOT want stored to persistence here.
    blacklist: ['login', 'search', 'nav'],
    // Optionally, just specify the keys you DO want stored to persistence.
    // An empty array means 'don't store any reducers' -> infinitered/ignite#409
    // whitelist: [],
    transforms: [immutablePersistenceTransform]
  }
}

export default REDUX_PERSIST
