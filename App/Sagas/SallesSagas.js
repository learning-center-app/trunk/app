/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import SallesActions from '../Redux/SallesRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'
// import { SallesSelectors } from '../Redux/SallesRedux'
import SortSallesByCampus from '../Transforms/SortSallesByCampus'
import SortReservationsBySalles from '../Transforms/SortReservationsBySalles'

export function * getSalles (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(SallesSelectors.getData)
  // make the call to the api
  const response = yield call(api.getSalles, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(SallesActions.sallesSuccess(response.data))
  } else {
    yield put(SallesActions.sallesFailure())
  }
}


export function * getSallesReservations (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(SallesSelectors.getData)
  // make the call to the api
  const response = yield call(api.getSallesReservations, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(SallesActions.sallesReservationsSuccess(response.data))
  } else {
    yield put(SallesActions.sallesReservationsFailure())
  }
}

export function * putSalleReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.putReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(SallesActions.putSalleReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(SallesActions.putSalleReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(SallesActions.putSalleReservationSoftFailure(response.data))
  }
}



export function * postSalleReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.postReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(SallesActions.postSalleReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(SallesActions.postSalleReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(SallesActions.postSalleReservationFailure(response.data))
  }
}

export function * deleteSalleReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.deleteReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(SallesActions.deleteSalleReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(SallesActions.deleteSalleReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(SallesActions.deleteSalleReservationFailure(response.data))
  }
}
