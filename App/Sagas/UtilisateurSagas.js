/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import UtilisateurActions, { UtilisateurSelectors } from '../Redux/UtilisateurRedux'

export function * login (api, action) {
  try {
    // Vérification que l'utilisateur n'est pas déjà connecté
    // const is_logged_in = yield select(UtilisateurSelectors.isUtilisateurLoggedIn)
    // Récupération de l'utilisateur
    const response = yield call(api.login)

    console.tron.log(response);

    if (response.ok) {
      yield put(UtilisateurActions.utilisateurSuccess(response.data))
    } else {
      if (!response.status){
        yield put(UtilisateurActions.utilisateurShowInternet())
      }
      yield put(UtilisateurActions.utilisateurFailure())
    }
  } catch (e) {
    console.tron.error(e);
  }
}
