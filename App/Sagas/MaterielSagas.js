/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import MaterielActions from '../Redux/MaterielRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'
// import { MaterielSelectors } from '../Redux/MaterielRedux'

export function * getMateriel (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(MaterielSelectors.getData)
  // make the call to the api
  const response = yield call(api.getMateriel, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(MaterielActions.materielSuccess(response.data))
  } else {
    yield put(MaterielActions.materielFailure())
  }
}


export function * getMaterielReservations (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(MaterielSelectors.getData)
  // make the call to the api
  const response = yield call(api.getMaterielReservations, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(MaterielActions.materielReservationsSuccess(response.data))
  } else {
    yield put(MaterielActions.materielReservationsFailure())
  }
}

export function * putMaterielReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.putReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(MaterielActions.putMaterielReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(MaterielActions.putMaterielReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(MaterielActions.putMaterielReservationSoftFailure(response.data))
  }
}



export function * postMaterielReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.postReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(MaterielActions.postMaterielReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(MaterielActions.postMaterielReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(MaterielActions.postMaterielReservationFailure(response.data))
  }
}

export function * deleteMaterielReservation (api, action) {
  //Extraction des données envoyées
  const { data } = action
  //On balance la requète
  const response = yield call(api.deleteReservation, data)

  // Ça roule ?
  if (response.ok) {
    //Mise à jour de l'état
    yield put(MaterielActions.deleteMaterielReservationSuccess(response.data))
  } else {
    //Vrai erreur serveur, non gérée
    if ([500, 403, 0].indexOf(response.status) >= 0){
      yield put(MaterielActions.deleteMaterielReservationFailure())
      if (!response.status)
        yield put(UtilisateurActions.utilisateurShowInternet())
    }
    //Sinon, on passe le message pour explication à l'utilisateur (dans .data)
    else
      yield put(MaterielActions.deleteMaterielReservationFailure(response.data))
  }
}
