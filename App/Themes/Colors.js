const colors = {
  background: 'rgb(18,13,10)',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  slightlyTransparentText: 'rgba(255,255,255,0.8)',
  text: '#FFFFFF',
  white: '#FFFFFF',
  darkText: '#222222',
  dark: 'rgb(17,15,16)',

  info:'rgb(31,167,226)',
  warning:'rgb(250,180,22)',
  error:'rgb(252,35,48)',
  success:'rgb(154,202,65)',

  greyFill: '#9e9e9e',
  alert:'#eb1414',
  valid:'#5fe004',

  navbar_button: 'rgb(155,202,65)',
  livres:'rgb(255, 104, 80)',
  livres_dark:'#240e0a',

  rdv:'#a55c5d',
  rdv_dark:'#1c1010'
}

export default colors
