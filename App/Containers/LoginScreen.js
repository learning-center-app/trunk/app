//@flow
import React, { Component } from 'react'
import { Platform, StatusBar, Text, View } from 'react-native'
import { WebView } from "react-native-webview"
import { connect } from 'react-redux'

import { getStatusBarHeight } from 'react-native-status-bar-height'

import SemanticButton from '../Components/SemanticButton'
import UtilisateurActions from '../Redux/UtilisateurRedux'

import API from '../Services/Api'

// Styles
import styles from './Styles/LoginScreenStyle'
//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

class LoginScreen extends Component {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};

    return {
      title: params.title,
    }

  }


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      color: 'white'
    };

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }

  handleEvent = (event) => {
    try {
      let data_from_html = event.nativeEvent.data
      //Si les données ont été encodée via encodeURIComponent via RN postMessage 
      // ce qui arrive depuis RN >= 0.59.10, pour une raison inexplicable
      if (data_from_html && data_from_html.match('%')){
        data_from_html = decodeURIComponent(decodeURIComponent(data_from_html))
      }

      //Récupération des données passées
      let data = JSON.parse(data_from_html);
      if(!data.error && data.utilisateur){
        let utilisateur = data.utilisateur;
        // console.tron.log(utilisateur);

        this.props.setUtilisateur(utilisateur);
        this.props.navigation.goBack();
        this.props.navigation.navigate("LoginCampusScreen", {error: 'no_campus'});
      }
    }
    catch(e) {
      console.tron.log(e);
    }
  }

  navigationStateChangedHandler = (event) => {
    console.tron.log(event)
  }

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, styles.inline, {width: '100%', paddingTop: getStatusBarHeight(true)}]}>
          <Text style={styles.sub_nav_text}>{translate('SCREEN.LOGIN.subtitle')}</Text>
          <SemanticButton
            text={translate('COMMON.retour')}
            type="success"
            size="small"
            styles={{marginRight:12}}
            onPress={()=> {this.props.setUtilisateurFailure(); this.props.navigation.goBack();}}
          />
        </View>
        <WebView
          source={{
            uri: API.baseURL + 'api/shibboleth/login', headers: {
              'Cache-Control': 'no-cache',
              'Accept-Language': i18n.locale
            }}}
          style={styles.webview}
          onMessage={this.handleEvent}
          onNavigationStateChange={this.navigationStateChangedHandler}

        />


      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    // html_mentions_legales : state.html.html_mentions_legales,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))},
    setUtilisateur : (utilisateur) => {dispatch(UtilisateurActions.utilisateurSuccess(utilisateur))},
    setUtilisateurFailure : () => {dispatch(UtilisateurActions.utilisateurFailure())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
