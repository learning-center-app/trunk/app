import React, { Component } from 'react'
import { Platform, View, ActivityIndicator, Dimensions, Linking } from 'react-native'
import ViewPager from '@react-native-community/viewpager';
import { WebView } from "react-native-webview"
import {Pages} from 'react-native-pages'
import PageControl from 'react-native-page-control'
import { connect } from 'react-redux'


// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
//API
import API from '../Services/Api'

import HtmlActions from '../Redux/HtmlRedux'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Internationalisation
import { translate } from '../I18n'

// Styles
import styles from './Styles/ActualitesScreenStyle'

class ActualitesScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })
  constructor(props){
    super(props);

    this.page = 1
    this.index_actu_encours = 0
    this.actualites = []
    this.api = API.create();
    this.screenWidth = Dimensions.get('window').width

    this.state={
      ...this.props.navigation.state.params,
      htmlActualite: "",
      isLoading: false,
      actualites: [],
      currentPage: 0,
      numberOfPages: 0
    }
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    this.actualites = this.state.actualites

    this.getUrlsActualites(this.page)
  }

  getUrlsActualites = (page) => {

    this.api.getActualites(page).then(apiDatas => {//récupération du 1er lot d'actualités (liste d'urls)
      if(apiDatas.ok === true){
        apiDatas.data.map((url) => {
          this.actualites = [...this.actualites, {url: url, html: '' }]
        })
      }
      console.tron.log(apiDatas)
      // else{
      //   if(page === 1 && this.props.html_actualites)//si 1ère page et échec de connexion, on affiche au moins l'html sauvegardé en mémoire
      //     this.actualites = [...this.actualites, {url: '', html: this.props.html_actualites }]
      // }

      this.setState({
        actualites: this.actualites,
        htmlActualite: this.props.html_actualites,
        isLoading: false,
        numberOfPages: this.actualites.length
      })

      if(this.actualites.length > 0 && this.index_actu_encours == 0){
        this.getActualite(this.index_actu_encours)//chargement de la 1ère actualité par défaut
      }
    })
  }

  getActualite = (num_actu) => {
    const index_actu_encours = this.index_actu_encours

    if(!this.actualites[num_actu].html)//si l'html déjà chargé, on ne fait rien
    {
      this.api.getHtml(this.actualites[num_actu].url).then(apiData => {
          if(apiData.ok === true){

            var html = apiData.data
            this.actualites[num_actu] = {...this.actualites, html: html}//ajout de l'html dans le tableau actualités
            //
            // if(num_actu == 0)//on enregistre toujours la 1ère actualité en mémoire
            //   this.props.setHtmlActualites(html)

            this.setState({
                actualites: this.actualites //mise à jour du state
            })
          }
      })
    }

    //A chaque fois, on va charger l'actualité suivant l'actualité en cours
    if(num_actu === index_actu_encours && num_actu < this.state.actualites.length - 1)
      this.getActualite(index_actu_encours + 1)

    //Si on arrive au bout des actualités affichées, alors on charge les urls suivantes (page + 1)
    if(index_actu_encours === this.state.actualites.length - 1)
    {
      this.page++
      this.getUrlsActualites(this.page)
    }
  }

  handleLinks = (event) => {
    this.webview.stopLoading();
    Linking.openURL(event.url);
  }
  displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  displayPageControl = () => {
    if (!this.state.isLoading) {
      return (
        <PageControl
          style={styles.pagecontrol_container}
          numberOfPages={this.state.numberOfPages}
          currentPage={this.state.currentPage}
          hidesForSinglePage
          pageIndicatorTintColor='white'
          currentPageIndicatorTintColor='rgb(155,202,65)'
          indicatorStyle={{borderRadius: 0}}
          currentIndicatorStyle={{borderRadius: 0}}
          indicatorSize={{width:8, height:8}}
          onPageIndicatorPress={this.onItemTap}
        />
      )
    }
  }

  displayPagerActualites = () => {
    if (!this.state.isLoading) {
      if(Platform.OS === 'ios'){
        return (
          <Pages
            style={[styles.content_container, {backgroundColor: this.state.color, width: this.screenWidth}]}
            indicatorColor='#000'
            indicatorPosition='none'
            startPage={0}
            onScrollEnd={(index) => {this.changePage(index)}}>
            { this.displayActualites() }
          </Pages>
        )
      }else{
        return (
          <ViewPager
            style={[styles.content_container, {backgroundColor: this.state.color, width: this.screenWidth}]}
            initialPage={0}
            onPageSelected={(e) => {this.changePage(e.nativeEvent.position)}}
          >
            {this.displayActualites()}
          </ViewPager>
        )
      }
    }
  }

  displayActualites = () => {
    return this.state.actualites.map((actu, i) => {
              if(!actu.html){
                return (
                  <View key={i+1} style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                  </View>
                )
              }else{
                if(Platform.OS === 'ios'){
                  return (
                    <WebView   originWhitelist={['*']} ref={(ref) => { this.webview = ref; }} style={styles.actuWebbox} source={{html: actu.html}} />
                  )
                }else{
                  return (
                    <View key={i+1}>
                      <WebView   originWhitelist={['*']} ref={(ref) => { this.webview = ref; }} style={styles.actuWebbox} source={{html: actu.html}} />
                    </View>
                  )
                }
              }
            })
  }

  changePage = (new_page) => {
    if (this.state.currentPage !== new_page) {
      this.setState({currentPage: new_page});
      this.getActualite(new_page)//à chaque changemet du pager, on va chercher l'html de l'actualité en cours d'affichage
    }
  }

  render () {
    return (
      <View style={styles.main_container}>
          {this.displayLoading()}
          {this.displayPageControl()}
          {this.displayPagerActualites()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    html_actualites : state.html.html_actualites
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setHtmlActualites : (html)=> {dispatch(HtmlActions.setHtmlActualites(html))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActualitesScreen)
