//@flow
import React, { Component } from 'react'
import { Platform, StatusBar, ScrollView, Text, View } from 'react-native'
import { connect } from 'react-redux'

import { getStatusBarHeight } from 'react-native-status-bar-height'

import SemanticButton from '../Components/SemanticButton'
import Picker from '../Components/Picker'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// Styles
import styles from './Styles/ModifierLangueScreenStyle'
import { Colors } from '../Themes'
//Internationalisation
import { translate, available_languages, setI18nConfig } from '../I18n'

class ModifierLangueScreen extends Component {
  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      code_langue : props.langue ? props.langue.id : null,
      color: Colors.background,
    };

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }

  setLangue = () => {
    //Si la langue sélectionnée est bien dispo, on get un objet
    //Si mode auto (ou choix inconnu dans les traductions dispo), on a undefined || null
    let _langue = available_languages.find(langue => { return langue.languageTag == this.state.code_langue}) || null;
    //On enregistre la langue dans redux pour qu'elle soit utilisée au startup
    this.props.setLangue(_langue);
    //On set la langue direct, ce qui trigger dans App.js
    setI18nConfig(_langue);
    //On fly
    this.props.navigation.goBack();

  }

  /** Traduit les options récupérées du redux via i18n */
  renderLocalizedOptions = () => {
    let langues = translate('LABEL_MAP.LANGUE');
    return Object.keys(langues)
      .map((key) => ({ value: key, label: langues[key] }))
  }
  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, { paddingTop: getStatusBarHeight(true) }]}><Text style={styles.sub_nav_text}>{translate('SCREEN.LANGUE.subtitle')}</Text></View>
        <ScrollView>
          <View>
            <Text style={[styles.label, styles.padded, styles.align_center]}>
              {translate('SCREEN.LANGUE.description')}
            </Text>
          </View>
          <View>
            <Picker
              placeholder={{
                label: translate('FORM.PLACEHOLDER.langue'),
                value:null
              }}
              items={this.renderLocalizedOptions()}
              onValueChange={(value, index) => this.setState({code_langue : value})}
              value={this.state.code_langue}
              />
          </View>
          <View style={[styles.actions]}>
            <SemanticButton
              text={translate('COMMON.annuler')}
              type="warning"
              onPress={()=>this.props.navigation.goBack()}
            />
            <SemanticButton
              text={translate('COMMON.valider')}
              type="success"
              onPress={()=>this.setLangue()}
            />
          </View>
        </ScrollView>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    langue : state.utilisateur.langue,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))},
    setUtilisateur : (utilisateur) => {dispatch(UtilisateurActions.utilisateurSuccess(utilisateur))},
    setLangue : (langue) => {dispatch(UtilisateurActions.setLangue(langue))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModifierLangueScreen)
