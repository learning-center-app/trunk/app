//@flow
import React from 'react'
import { View, FlatList, Text, Image, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import { Calendar } from 'react-native-calendars'

import Modal from '../Components/Modal'
import Materiel from '../Classes/Materiel'

import moment from 'moment'
import 'moment/locale/fr'
import 'moment-round'
//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

import AvailableLabel from '../Components/AvailableLabel'
import ReservationItem from '../Components/ReservationItem'
import SemanticButton from '../Components/SemanticButton'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

import PutFilteredReservationsInItem from '../Transforms/PutFilteredReservationsInItem'

//Actions redux
import MaterielActions, { MaterielSelectors } from '../Redux/MaterielRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'


// More info here: https://facebook.github.io/react-native/docs/sectionlist.html

// Styles
import styles from './Styles/MesReservationsMaterielScreenStyle'
import { ApplicationStyles, Images, Fonts } from '../Themes'

class MesReservationsMaterielScreen extends React.PureComponent {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,

    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095
    headerRight: <TouchableOpacity style={styles.navbar_button} onPress={() => {navigation.navigate('ReservationMaterielScreen', navigation.state.params)}}>
                  <Image source={Images.actions_icons.dark.add} style={styles.navbar_icon}></Image>
                </TouchableOpacity>
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {...props.navigation.state.params};

    this.state.modal = {
      edit: false,
    }
    // this.state.data = new Materiel(this.DEFAULT_DATA);

    console.tron.log(this.state);
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }

  componentDidMount () {
    //Chopage des résas de materiel au cas où

    //Si l'utilisateur est pas connecté, on pop le login
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    else {
      this.props.materielReservationsRequest();
    }
    // console.tron.log(this.props.sorted_by_campus)
  }

  //https://stackoverflow.com/a/44750890/1437016
  componentDidUpdate(prevProps, prevState, snapshot) {

      //Affichage de l'erreur
      if (prevProps.sending_error !== this.props.sending_error
          && this.props.sending_error && !this.state.showing_error) {
        //Flag dans le state pour éviter les multiples affichages
        this.setState((partialState)=>({...partialState, showing_error:true}), ()=>{
          //Obligé de mettre un timeout, car conflit entre modal et alert : https://github.com/facebook/react-native/issues/10471
          setTimeout(()=>{
            if(typeof this.props.sending_error == "object")
              Alert.alert(
                translate('ERROR.erreur'),
                this.props.sending_error.error,
                [{ text: 'OK', onPress: () => this.setState((partialState) => ({ showing_error: false })) }],
                { cancelable: false }
              );
            else
              Alert.alert(
                translate('ERROR.erreur'),
                translate('ERROR.indisponible'));
          }, 600)
        })
      }
  }

  DEFAULT_DATA = {
    start_time : moment(),
    end_time : moment(),
  }

  //Options pour le modal
  //Définitions des boutons
  EDIT_BUTTONS = [
    {
      text: translate('COMMON.supprimer'),
      type : 'error',
      onPress : () => {
        //Appel à l'API via Redux
        this.props.deleteMaterielReservationRequest({
          ...this.state.data,
          start_time: this.state.data.start_time.unix(),
          end_time: this.state.data.end_time.unix(),
          getAvailability:null,
        });
        //Hide du modal
        this.setState((partialState)=> ({
          ...partialState,
          modal : {
            edit: false
          }
        }))
      }
    },
    {
      text: translate('COMMON.modifier'),
      type : 'success',
      onPress : () => {
        //Vérification de la disponibilité du matériel.
        let _availability = this.getReservationsOfMateriel(this.state.data).getAvailability(this.props.utilisateur, this.state.data.start_time, this.state.data.end_time, true)
        console.tron.log(_availability);

        if(_availability != "available")
          return;

        console.tron.log({
          ...this.state.data,
          start_time: this.state.data.start_time.unix(),
          end_time: this.state.data.end_time.unix(),
        })
        //Appel à l'API via Redux
        this.props.postMaterielReservationRequest({
          ...this.state.data,
          start_time: this.state.data.start_time.unix(),
          end_time: this.state.data.end_time.unix(),
        });
        //Hide du modal
        this.setState((partialState)=> ({
          ...partialState,
          modal : {
            edit: false
          }
        }))
      }
    }
  ]

  setDay = (day, type) => {
    // console.tron.log({day})
    let _data = {...this.state.data};
    //Affectation des heures dans le jour sélectionné
    _data[type].set({
      year: day.year,
      month: day.month -1,
      date: day.day
    })

    //Attention à ne pas avoir un date de début postérieur à la date de fin
    if (_data.end_time.isSameOrBefore(_data.start_time)) {
      //si on est en train d'avancer l'heure de début, on avance l'heure de fin
      if (type == 'start_time')
        _data.end_time = _data.start_time.clone().add(1, 'day')
      //si on est en train de reculer l'heure de fin, on recule l'heure de début
      else
        _data.start_time = _data.end_time.clone().subtract(1, 'day')
    }

    this.setState({
      data: {
        ..._data,
        start_time: _data.start_time,
        end_time: _data.end_time,
      }
    })
  }

  setAM = (time, type, hour = 9) => {
    this._updateHours('hours', hour, time, type)
  }

  setPM = (time, type, hour = 14) => {
    this._updateHours('hours', hour, time, type)
  }
  //Privates
  //Helper
  _updateHours = (operation = 'add', number = 9, time, type) => {
    time = moment.isMoment(time) ? time.clone() : moment(time);
    time[operation](number);
    //Copy des filtres actuels
    let _data = {...this.state.data};
    _data[type] = time;

    this.setState({
      data: {
        ..._data,
        start_time: _data.start_time.clone(),
        end_time: _data.end_time.clone(),
      }
    })
  }
  getCreneauFormat = (date) => {
    return date.format('A');
  }

  showModalEdit = (item) => {
    console.tron.log(item)
    this.setState((partialState)=> ({
      ...partialState,
      data : new Materiel({
        ...item,
        start_time : moment.unix(item.start_time),
        end_time : moment.unix(item.end_time),
      }),
      modal : {
        edit: true
      },
      // data : item
    }))
  }

  hideModalEdit = (item) => {
    this.setState((partialState)=> ({
      modal : {
        edit: false
      },
      data : new Materiel(this.DEFAULT_DATA)
    }));
  }

  handlers = {
    onReserved : this.showModalEdit,
    onAvailable : () => { },
    onConflict : ()=>{ },
    onUnavailable : ()=>{ },
  }

  renderItem = ({item}) => {
    return (
      <ReservationItem backgroundColor={this.state.color} types_reservation={this.props.types_reservation} item={item} handlers={this.handlers}></ReservationItem>
    )
  }

  // Conditional branching for section headers, also see step 3
  renderSectionHeader ({section}) {
    return <Text style={styles.sectionFixedHeader}>{section.name}</Text>
  }


  getReservationsOfMateriel = (materiel) => {
    let _materiel = PutFilteredReservationsInItem(this.props.all_reservations, materiel, Materiel);
    return _materiel;
  }
  // Show this when data is empty
  renderEmpty = () =>
    <View>
      <Text style={styles.list_empty}>{translate('COMMON.aucune_reservation')}</Text>
      <View style={styles.inline}>
        <SemanticButton onPress={() => { this.props.navigation.navigate('ReservationMaterielScreen', this.props.navigation.state.params) }} text={translate('COMMON.nouvelle_reservation')} type='success'>
        </SemanticButton>
      </View>
    </View>

  renderSeparator = () =>
    <View style={styles.row_separator}/>

  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => {item.id}

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 20

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('COMMON.mes_reservations')}</Text></View>
        <FlatList
          // renderSectionHeader={this.renderSectionHeader}
          data={this.props.sorted_by_campus}
          contentContainerStyle={styles.listContent}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          initialNumToRender={this.oneScreensWorth}
          stickySectionHeadersEnabled={false}
          ListHeaderComponent={this.renderHeader}
          // SectionSeparatorComponent={this.renderSectionSeparator}
          // ListFooterComponent={this.renderFooter}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
          // renderSectionFooter={this.renderSectionFooter}
          extraData={this.props}
        />
        {!!this.state.data && (
          <Modal
            isVisible={this.state.modal.edit}
            title={translate('MODAL.JOURS.modifier')}
            onCancel={this.hideModalEdit}
            buttons={this.EDIT_BUTTONS}>
            <Calendar
              // Initially visible month. Default = Date()
              current={this.state.data.end_time.toDate()}
              //Montrer le jour en cours
              markedDates={{[this.state.data.end_time.format('YYYY-MM-DD')]: {selected: true}}}
              // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
              minDate={moment().isBefore(this.state.data.start_time) ? this.state.data.start_time.toDate() : Date()}
              // Handler which gets executed on day press. Default = undefined
              onDayPress={(day)=>this.setDay(day, 'end_time')}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={'MMMM yyyy'}
              // Do not show days of other months in month page. Default = false
              hideExtraDays={true}
              // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
              firstDay={1}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              onPressArrowLeft={substractMonth => substractMonth()}
              // Handler which gets executed when press arrow icon left. It receive a callback can go next month
              onPressArrowRight={addMonth => addMonth()}
              theme={ApplicationStyles.calendar}
              />

              <View style={[styles.modal_block, {marginVertical:0}]}>
                <View style={styles.modal_inline}>
                  <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setAM(this.state.data.end_time, 'end_time', 12)}>
                    <Text style={Fonts.style.h1}>-</Text>
                  </TouchableOpacity>
                  <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.getCreneauFormat(this.state.data.end_time)}</Text>
                  <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setPM(this.state.data.end_time, 'end_time', 18)}>
                    <Text style={Fonts.style.h1}>+</Text>
                  </TouchableOpacity>
                </View>
                <AvailableLabel status={this.getReservationsOfMateriel(this.state.data).getAvailability(this.props.utilisateur, this.state.data.start_time, this.state.data.end_time, true)}/>
              </View>
          </Modal>)
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sorted_by_campus : MaterielSelectors.getReservationsByUser(state),
    utilisateur : state.utilisateur,
    types_reservation : state.materiel.configuration.types_reservation,
    all_reservations : state.materiel.reservations.raw,
    sending_error : state.materiel.reservations.sending_error,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    materielReservationsRequest: ()=>{dispatch(MaterielActions.materielReservationsRequest())},
    postMaterielReservationRequest: (data)=>{dispatch(MaterielActions.postMaterielReservationRequest(data))},
    deleteMaterielReservationRequest: (data)=>{dispatch(MaterielActions.deleteMaterielReservationRequest(data))},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MesReservationsMaterielScreen)
