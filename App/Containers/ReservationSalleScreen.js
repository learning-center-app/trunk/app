//@flow
import React from 'react'
import { View, SectionList, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import moment from 'moment'
import 'moment-round'

import ListItem from '../Components/ListItem'

import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Actions redux
import SallesActions, { SallesSelectors } from '../Redux/SallesRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

import { Calendar } from 'react-native-calendars'
// More info here: https://facebook.github.io/react-native/docs/sectionlist.html

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

// Styles
import styles from './Styles/ReservationSalleScreenStyle'
import { ApplicationStyles, Images, Fonts } from '../Themes'

class ReservationSalleScreen extends React.PureComponent {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)}/>,
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      filters: {
        start_time: moment().ceil(30, 'minutes'),
        end_time: moment().add(60, 'minutes').ceil(30, 'minutes')
      },
      modal: {
        calendar: false,
        hours: false,
      }
    };
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }
  componentDidMount () {
    //Chopage des résas de salles au cas où
    // this.props.sallesReservationsRequest();
  }


  getDateFormat = (start, end) => {
    //Si c'est le même jour
    if (start.isSame(end, 'day'))
      return `${start.format('ddd D MMM YYYY')}`
    else
      return `${translate('COMMON.du')} ${start.format('ddd D MMM YYYY')}\n${translate('COMMON.au')} ${end.format('ddd D MMM YYYY')}`
  }
  getHoursFormat = (start, end) => {
    return `${start.format('H[h]mm')} > ${end.format('H[h]mm')}`
  }

  /* MODAL CALENDRIER */
  showCalendar = () => {
    this.setState({
      modal:{
        calendar:true,
      }
    })
  }

  hideCalendar = () => {
    this.setState({
      modal:{
        calendar:false,
      }
    })
  }

  setDay = (day) => {
    console.tron.log({day})
    let current_filters = this.state.filters,
        _start_time = moment(day.dateString),
        _end_time = moment(day.dateString);
    //Affectation des heures en cours dans le jour sélectionné
    _start_time.hours(current_filters.start_time.hours()).minutes(current_filters.start_time.minutes())
    _end_time.hours(current_filters.end_time.hours()).minutes(current_filters.end_time.minutes())

    this.setState({
      filters: {
        start_time: _start_time,
        end_time: _end_time,
      }
    })

    this.hideCalendar()
  }
  //Options pour le modal
  //Définitions des boutons
  CALENDAR_BUTTONS = [
    {
      text : translate('COMMON.annuler'),
      type : 'info',
      onPress : this.hideCalendar
    }
  ]


  /******** MODAL Heures *********/
  showHours = () => {
    this.setState({
      modal:{
        hours:true,
      }
    })
  }

  hideHours = () => {
    this.setState({
      modal:{
        hours:false,
      }
    })
  }

  subtract30minutes = (time, type) => {
    this._updateMinutes('subtract', 30, 'minutes', time, type)
  }

  add30minutes = (time, type) => {
    this._updateMinutes('add', 30, 'minutes', time, type)
  }
  //Privates
  //Helper
  _updateMinutes = (operation = 'add', number = 30, unity = 'minutes', time, type) => {
    time = moment.isMoment(time) ? time.clone() : moment(time);
    time[operation](number, unity);
    this._setHours(time, type)
  }
  //time : objet moment
  //type : propriété à changer
  _setHours = (time, type = 'start_time') => {
    //Copy des filtres actuels
    let _filters = {...this.state.filters};
    //Affectation des heures dans le jour sélectionné
    _filters[type].hours(time.hours()).minutes(time.minutes())

    //Attention à ne pas avoir un date de début postérieur à la date de fin
    if (_filters.end_time.isSameOrBefore(_filters.start_time)) {
      //si on est en train d'avancer l'heure de début, on avance l'heure de fin
      if (type == 'start_time')
        _filters.end_time.add(30, 'minutes')
      //si on est en train de reculer l'heure de fin, on recule l'heure de début
      else
        _filters.start_time.subtract(30, 'minutes')
    }

    //Obligé de cloner l'objet pour que la comparaison par référence soit fausse et que ça recharge la vue
    this.setState({
      filters: {
        start_time: _filters.start_time.clone(),
        end_time: _filters.end_time.clone(),
      }
    })
  }

  //Options pour le modal
  //Définitions des boutons
  HOURS_BUTTONS = [
    {
      text : translate('COMMON.valider'),
      type : 'success',
      onPress : this.hideHours
    }
  ]

  //Handlers pour les items de la liste
  handlers = {
    onAvailable : (salle) => {
      this.props.navigation.navigate('ReservationSalleDetailScreen', {
        ...this.state,
        salle,
        filter_dates : {
          start_time : this.state.filters.start_time.unix(),
          end_time : this.state.filters.end_time.unix(),
        }
      })
    },
    // onConflict : ()=>{alert('Probleme')},
    // onUnavailable : ()=>{alert('Impossible')},
  }

  renderItem = ({section, item}) => {
    return (
      <ListItem
        backgroundColor={this.state.color}
        utilisateur={this.props.utilisateur}
        section={section}
        item={item}
        handlers={this.handlers}
        start_time={this.state.filters.start_time}
        end_time={this.state.filters.end_time}
        gender='feminine'
      ></ListItem>
    )
  }

  // Conditional branching for section headers, also see step 3
  renderSectionHeader ({section}) {
    return <Text style={styles.sectionFixedHeader}>{translate(`LABEL_MAP.CAMPUS.${section.campus_id}`)}</Text>
  }


  // Show this when data is empty
  renderEmpty = () =>
    <Text style={styles.list_empty}>{translate('COMMON.aucune_salle')}</Text>

  renderSeparator = () =>
    <View style={styles.row_separator}/>


  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => index

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 20

  render () {
    return (
      <View style={styles.container}>

        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('COMMON.nouvelle_reservation')}</Text></View>
        <View style={[styles.filter]}>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showCalendar}>
              <Text style={styles.filter_title}>{translate('COMMON.jour')}</Text>
              <Text style={styles.filter_text}>{this.getDateFormat(this.state.filters.start_time, this.state.filters.end_time)}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showHours}>
              <Text style={styles.filter_title}>{translate('COMMON.horaires')}</Text>
              <Text style={styles.filter_text}>{this.getHoursFormat(this.state.filters.start_time, this.state.filters.end_time)}</Text>
            </TouchableOpacity>
          </View>
          <Image source={Images.actions_icons.light.calendrier} style={styles.filter_icon}></Image>
        </View>
        {/* <View style={styles.filter}>
          <View style={styles.filter_block}>
            <Text style={styles.filter_title}>Options</Text>
            <Text style={styles.filter_text}>Campus de Nantes, moins de 10 places</Text>
          </View>
          <Image source={Images.actions_icons.light.filtres} style={styles.filter_icon}></Image>
        </View> */}
        <SectionList
          renderSectionHeader={this.renderSectionHeader}
          sections={this.props.sorted_by_campus}
          contentContainerStyle={styles.listContent}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          stickySectionHeadersEnabled={false}
          initialNumToRender={this.oneScreensWorth}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
          extraData={this.state.filters}
          refreshing={false}
          onRefresh={this.props.sallesReservationsRequest}
        />

        <Modal
          isVisible={this.state.modal.calendar}
          title={translate('MODAL.JOURS.selectionner')}
          buttons={this.CALENDAR_BUTTONS}>
          <Calendar
            // Initially visible month. Default = Date()
            current={this.state.filters.start_time.toDate()}
            //Montrer le jour en cours
            markedDates={{[this.state.filters.start_time.format('YYYY-MM-DD')]: {selected: true}}}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={Date()}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={this.setDay}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            theme={ApplicationStyles.calendar}
            />
        </Modal>
        <Modal
          isVisible={this.state.modal.hours}
          title={translate('MODAL.HEURES.selectionner')}
          buttons={this.HOURS_BUTTONS}>
          <View style={styles.modal_block}>
            <Text style={styles.modal_label}>{translate('MODAL.HEURES.heure_debut')}</Text>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.subtract30minutes(this.state.filters.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.state.filters.start_time.format('H[h]mm')}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.add30minutes(this.state.filters.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.modal_block}>
            <Text style={styles.modal_label}>{translate('MODAL.HEURES.heure_fin')}</Text>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.subtract30minutes(this.state.filters.end_time, 'end_time')}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.state.filters.end_time.format('H[h]mm')}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.add30minutes(this.state.filters.end_time, 'end_time')}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sorted_by_campus : SallesSelectors.getSallesSortedByCampus(state),
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    sallesReservationsRequest: ()=>{dispatch(SallesActions.sallesReservationsRequest())},
    putSalleReservationRequest: (data)=>{dispatch(SallesActions.putSalleReservationRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReservationSalleScreen)
