//@flow
import React, { Component } from 'react'
import { Platform, StatusBar, ScrollView, Text, View } from 'react-native'
import { WebView } from "react-native-webview"
import { connect } from 'react-redux'

import { getStatusBarHeight } from 'react-native-status-bar-height';

import SemanticButton from '../Components/SemanticButton'
import UtilisateurActions from '../Redux/UtilisateurRedux'

import API from '../Services/Api'

// Styles
import styles from './Styles/MentionsLegalesScreenStyle'

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

class MentionsLegalesScreen extends Component {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};

    return {
      title: params.title,
    }

  }


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
    console.tron.log({baseURL : API.baseURL})

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, { paddingTop: getStatusBarHeight(true) }]}><Text style={styles.sub_nav_text}>{translate("SCREEN_NAME_MAP.MentionsLegalesScreen")}</Text></View>
        <WebView
          source={{
            uri: API.baseURL + 'api/prismic/mentions-legales', headers: {
              'Cache-Control': 'no-cache',
              'Accept-Language': i18n.locale
            }}}
          style={styles.webview}
        />

        <View style={[styles.actions]}>

          <SemanticButton
            text={translate('COMMON.retour')}
            type="success"
            onPress={()=> this.props.navigation.goBack()}
          />
        </View>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    // html_mentions_legales : state.html.html_mentions_legales,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MentionsLegalesScreen)
