import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TextInput, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import SemanticButton from '../Components/SemanticButton'
import Picker from '../Components/Picker'
import HTMLView from '../Components/HTMLView';

import { Images, Fonts, Colors, ApplicationStyles } from '../Themes'

import AvailableLabel from '../Components/AvailableLabel'
import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

import MaterielActions, { MaterielSelectors } from '../Redux/MaterielRedux'

import moment from 'moment'
import 'moment/locale/fr'
import 'moment-round'
//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

//Mise à jour du AM en Matin et PM en Après-midi
import '../Transforms/TransformMeridiemLocale'

import { Calendar } from 'react-native-calendars'

// Styles
import styles from './Styles/ReservationMaterielDetailScreenStyle'

class ReservationMaterielDetailScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })
  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = props.navigation.state.params;

    this.state.data = {
      start_time : moment.unix(this.state.filter_dates.start_time), //Date de début
      end_time : moment.unix(this.state.filter_dates.end_time), //Date de fin = maintenant + 60 minutes
      room_id : this.state.materiel.id, //Identifiant du materiel
      user: this.props.utilisateur,
      description : translate('COMMON.description_reservation_defaut'),   //Description libre
      type : null   //Type de résa
    }
    this.state.modal = {
      calendar: false,
      hours: false,
    }
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.sending_error !== this.props.sending_error
        && this.props.sending_error) {
      if (typeof this.props.sending_error == "object")
        alert(`${translate('ERROR.indisponible_avec_raison')} ${this.props.sending_error.message}`);
      else
        alert(translate('ERROR.indisponible'));
    }
    //Mise à jour du state uniquement si la prop à changer
    //Et qu'il y a une réservation de faite
    if (prevProps.lastReservation !== this.props.lastReservation
        && this.props.lastReservation) {

      //On ne fait pas d'inversion du state interne : on prend directement la valeur du props
      this.setState((partialState)=> ({
        ...partialState,
        lastReservation : this.props.lastReservation
      }))
    }
  }

  //Options pour le modal
  //Définitions des boutons
  SUCCESS_RESERVATION_BUTTONS = [
    {
      text: translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        //NON : On retourne à l'écran "MesReservationsScreen", qui était passé en params tout ce temps là
        this.props.navigation.goBack(null);
        this.setState((partialState)=> ({
          ...partialState,
          lastReservation : null
        }))
      }
    }
  ]


  getDateDisplay = (item) => {
    var start = moment.unix(item.start_time).locale('fr'),
          end = moment.unix(item.end_time).locale('fr');

    //Si c'est le même jour
    if (start.isSame(end, 'day'))
      return `${start.format('dddd LL')} ${translate('COMMON.de')} ${start.format('H:mm')} ${translate('COMMON.a')} ${end.format('H:mm')}`
    else
      return `${translate('COMMON.du')} ${start.format('dddd LL')} à ${start.format('H:mm')}\n${translate('COMMON.au')} ${end.format('dddd LL')} ${end.format('H:mm')}`
  }

  //Texte à afficher à l'intérieur du modal (récupéré via this.props.children dans Modal)
  modalText = () => {
    if (this.state.lastReservation)
      return `${this.state.lastReservation.materiel} ${translate('COMMON.reserve')}\n ${this.getDateDisplay(this.state.lastReservation)})`
    else
      return ''
  }

  getDateFormat = (date) => {
    return date.format('ddd D MMM YYYY');
  }
  getCreneauFormat = (date) => {
    return date.format('A');
  }

  /* MODAL CALENDRIER */
  showCalendarStart = () => {
    this.setState({
      modal:{
        calendar_start:true,
      }
    })
  }

  showCalendarEnd = () => {
    this.setState({
      modal:{
        calendar_end:true,
      }
    })
  }

  hideCalendar = () => {
    this.setState({
      modal:{
        calendar_start:false,
        calendar_end:false,
      }
    })
  }

  setDay = (day, type) => {
    console.tron.log({day})
    let _data = {...this.state.data};
    //Affectation des heures dans le jour sélectionné
    _data[type].set({
      year: day.year,
      month: day.month -1,
      date: day.day
    })

    //Attention à ne pas avoir un date de début postérieur à la date de fin
    if (_data.end_time.isSameOrBefore(_data.start_time)) {
      //si on est en train d'avancer l'heure de début, on avance l'heure de fin
      if (type == 'start_time')
        _data.end_time = _data.start_time.clone().add(1, 'day')
      //si on est en train de reculer l'heure de fin, on recule l'heure de début
      else
        _data.start_time = _data.end_time.clone().subtract(1, 'day')
    }
    this.setState({
      data: {
        ...this.state.data,
        start_time: _data.start_time,
        end_time: _data.end_time,
      }
    })

    this.hideCalendar()
  }
  //Options pour le modal
  //Définitions des boutons
  CALENDAR_BUTTONS = [
    {
      text: translate('COMMON.annuler'),
      type : 'warning',
      onPress : this.hideCalendar
    }
  ]


  /******** MODAL Heures *********/
  showHoursStart = () => {
    this.setState({
      modal:{
        hours_start:true,
      }
    })
  }

  showHoursEnd = () => {
    this.setState({
      modal:{
        hours_end:true,
      }
    })
  }

  hideHours = () => {
    this.setState({
      modal:{
        hours_start:false,
        hours_end:false,
      }
    })
  }

  setAM = (time, type, hour = 9) => {
    this._updateHours('hours', hour, time, type)
  }

  setPM = (time, type, hour = 14) => {
    this._updateHours('hours', hour, time, type)
  }
  //Privates
  //Helper
  _updateHours = (operation = 'add', number = 9, time, type) => {
    time = moment.isMoment(time) ? time.clone() : moment(time);
    time[operation](number);
    //Copy des filtres actuels
    let _data = {...this.state.data};
    _data[type] = time;

    this.setState({
      data: {
        ...this.state.data,
        start_time: _data.start_time,
        end_time: _data.end_time,
      }
    })
  }

  //Options pour le modal
  //Définitions des boutons
  HOURS_BUTTONS = [
    {
      text: translate('COMMON.valider'),
      type : 'success',
      onPress : this.hideHours
    }
  ]
  /** Traduit les options récupérées du redux via i18n */
  renderLocalizedTypeOptions = () => {
    return this.props.types_reservation_select.map(({ label, value }) => ({ value, label: translate(`LABEL_MAP.TYPE_RESERVATION.${value}`) }))
  }

  //Override pour transformer les données avant envoi
  putMaterielReservationRequest = (data) => {
    //Clone de l'objet
    let _data = {...data};
    //Conversion des objets moment en timestamps (en secondes)
    _data.start_time = _data.start_time.unix();
    _data.end_time = _data.end_time.unix();
    this.props.putMaterielReservationRequest(_data);
  }

  render () {
    let _availability = this.state.materiel.getAvailability(this.props.utilisateur, this.state.data.start_time, this.state.data.end_time)
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('COMMON.nouvelle_reservation')}</Text></View>
        <ScrollView>
          <View style={[styles.inline, styles.padded, styles.header]}>
            {
              //Cast en booléen pour éviter erreur : https://github.com/facebook/react-native/issues/18773#issuecomment-380237027
              //Désactivé volontairement
              false && !!this.state.materiel.image && <Image source={{uri: this.state.materiel.image}} style={styles.illustration} />
            }
            <View style={styles.box}>
              <Text style={Fonts.style.h2}>{this.state.materiel.room_name}</Text>
              <View style={[styles.inline]}>
                <View style={[styles.param]}>
                  <Text style={[styles.label]}>{translate('SCREEN.MATERIEL.date_debut')}</Text>
                  <TouchableOpacity onPress={()=>this.showCalendarStart()} style={styles.param_block}>
                    <Image source={Images.actions_icons.light.calendrier} style={styles.param_icon}></Image>
                    {/* <Text style={[styles.label, styles.param_title]}>Jour</Text> */}
                    <Text style={styles.param_text}>{this.getDateFormat(this.state.data.start_time)}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.showHoursStart()} style={[styles.param_block, styles.param_with_margin]}>
                    <Image source={Images.actions_icons.light.horaires} style={styles.param_icon}></Image>
                    {/* <Text style={[styles.label, styles.param_title]}>Horaires</Text> */}
                    <Text style={styles.param_text}>{this.getCreneauFormat(this.state.data.start_time)}</Text>
                  </TouchableOpacity>
                </View>
                <View style={[styles.param]}>
                  <Text style={[styles.label]}>{translate('SCREEN.MATERIEL.date_fin')}</Text>
                  <TouchableOpacity onPress={()=>this.showCalendarEnd()} style={styles.param_block}>
                    <Image source={Images.actions_icons.light.calendrier} style={styles.param_icon}></Image>
                    {/* <Text style={[styles.label, styles.param_title]}>Jour</Text> */}
                    <Text style={styles.param_text}>{this.getDateFormat(this.state.data.end_time)}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.showHoursEnd()} style={[styles.param_block, styles.param_with_margin]}>
                    <Image source={Images.actions_icons.light.horaires} style={styles.param_icon}></Image>
                    {/* <Text style={[styles.label, styles.param_title]}>Horaires</Text> */}
                    <Text style={styles.param_text}>{this.getCreneauFormat(this.state.data.end_time)}</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <AvailableLabel status={_availability}/>
            </View>
          </View>
          <View>
            <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.description_courte')}</Text>
            <TextInput
              multiline={true}
              onChangeText={(description) => this.setState((state, props) => ({data : {...state.data, description: description}}))}
              placeholder={translate('FORM.PLACEHOLDER.description_courte')}
              editable = {true}
              maxLength = {200}
              underlineColorAndroid='transparent'
              placeholderTextColor = {Colors.slightlyTransparentText}
              style={[styles.input, styles.padded]}
            />
          </View>
          <View>
            <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.type')}</Text>
            <Picker
              placeholder={{
                label: translate('FORM.PLACEHOLDER.type'),
                value:null
              }}
              items={this.renderLocalizedTypeOptions()}
              onValueChange={(value, index) => this.setState((state, props) => ({data : {...state.data, type: value}}))}
              value={this.state.data.type}
            />
          </View>
          <View style={styles.padded}>
            <Text style={[styles.label]}>{translate('FORM.LABEL.information_materiel')}</Text>
            <View style={styles.informations}>
              <Text style={[Fonts.style.description]}>{this.state.materiel.campus}</Text>
              <Text style={[Fonts.style.description]}>{this.state.materiel.description}</Text>
              { !!this.state.materiel.comment_room && <HTMLView value={this.state.materiel.comment_room}></HTMLView>}
            </View>
          </View>
          <View style={styles.actions}>
            <SemanticButton
              text={translate('COMMON.reserver')}
              disabled={_availability != "available"}
              onPress={()=>{this.putMaterielReservationRequest(this.state.data)}}
              type="success"
            />
          </View>
        </ScrollView>

        <Modal
          isVisible={this.state.lastReservation}
          title={translate('MODAL.RESERVATION.SUCCESS.title')}
          onCancel={this.hideModal}
          buttons={this.SUCCESS_RESERVATION_BUTTONS}>
          <Text style={{...Fonts.style.normal}}>{this.modalText()}</Text>
        </Modal>
        <Modal
          isVisible={this.state.modal.calendar_start}
          title={translate('MODAL.JOURS.selectionner_debut')}
          buttons={this.CALENDAR_BUTTONS}>
          <Calendar
            // Initially visible month. Default = Date()
            current={this.state.data.start_time.toDate()}
            //Montrer le jour en cours
            markedDates={{[this.state.data.start_time.format('YYYY-MM-DD')]: {selected: true}}}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={Date()}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day)=>this.setDay(day, 'start_time')}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            theme={ApplicationStyles.calendar}
            />
        </Modal>

        <Modal
          isVisible={this.state.modal.calendar_end}
          title={translate('MODAL.JOURS.selectionner_fin')}
          buttons={this.CALENDAR_BUTTONS}>
          <Calendar
            // Initially visible month. Default = Date()
            current={this.state.data.end_time.toDate()}
            //Montrer le jour en cours
            markedDates={{[this.state.data.end_time.format('YYYY-MM-DD')]: {selected: true}}}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={Date()}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day)=>this.setDay(day, 'end_time')}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            theme={ApplicationStyles.calendar}
            />
        </Modal>
        <Modal
          isVisible={this.state.modal.hours_start}
          title={translate('MODAL.CRENEAU.selectionner_debut')}
          buttons={this.HOURS_BUTTONS}>
          <View style={styles.modal_block}>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setAM(this.state.data.start_time, 'start_time', 9)}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.getCreneauFormat(this.state.data.start_time)}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setPM(this.state.data.start_time, 'start_time', 14)}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          isVisible={this.state.modal.hours_end}
          title={translate('MODAL.CRENEAU.selectionner_debut')}
          buttons={this.HOURS_BUTTONS}>
          <View style={styles.modal_block}>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setAM(this.state.data.end_time, 'end_time', 12)}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.getCreneauFormat(this.state.data.end_time)}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setPM(this.state.data.end_time, 'end_time', 18)}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    utilisateur : state.utilisateur,
    sending : state.materiel.reservations.sending,
    sending_error : state.materiel.reservations.sending_error,
    lastReservation : state.materiel.reservations.lastReservation,
    types_reservation_select : MaterielSelectors.getTypesReservationsForSelect(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    putMaterielReservationRequest: (data)=>{dispatch(MaterielActions.putMaterielReservationRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReservationMaterielDetailScreen)
