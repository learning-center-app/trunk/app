import React, { Component } from 'react'
import { Platform, View, ActivityIndicator, Dimensions, Linking } from 'react-native'
import ViewPager from '@react-native-community/viewpager';
import { WebView } from "react-native-webview"
import {Pages} from 'react-native-pages'
import PageControl from 'react-native-page-control'
import { connect } from 'react-redux'

//API
import API from '../Services/Api'

import HtmlActions from '../Redux/HtmlRedux'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

// Styles
import styles from './Styles/DescriptionScreenStyle'

class DescriptionScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={navigation.state.params.title}/>,
  })
  constructor(props){
    super(props);
    this.api = API.create()
    this.index_description_encours = 0
    this.screenWidth = Dimensions.get('window').width

    this.state = {
      ...this.props.navigation.state.params,
      isLoading: true,
      array_description: [],
      currentPage: 0,
      numberOfPages: 0
    }
  }

  componentDidMount() {
    //Récup initiale
    this.getUrlsDescription()
  }

  //Récupération initiale de toutes les pages de description
  getUrlsDescription = () => {
    this.api.getDescription().then(apiDatas => {//récupération du 1er lot d'descriptions (liste d'urls)
      if(apiDatas.ok === true){
        let _array_description = apiDatas.data.map((url) => {
          return {url: url, html: null }
        })

        this.setState({
          array_description: _array_description,
          isLoading: false,
          numberOfPages: _array_description.length
        })

        if(_array_description.length > 0 && this.index_description_encours == 0){
          this.getDescription(this.index_description_encours)//chargement de la 1ère description par défaut
        }
      }
      else {
        alert("Vous devez être connecté")
      }
      // else{
      //   if(page === 1 && this.props.html_description)//si 1ère page et échec de connexion, on affiche au moins l'html sauvegardé en mémoire
      //     this.state.array_description = [...this.state.array_description, {url: '', html: this.props.html_description }]
      // }

    })
  }

  getDescription = (num_description) => {
    const index_description_encours = this.index_description_encours

    if(!this.state.array_description[num_description].html)//si l'html déjà chargé, on ne fait rien
    {
      this.api.getHtml(this.state.array_description[num_description].url).then(apiData => {
        if(apiData.ok === true){

          var html = apiData.data,
              _array_description = [...this.state.array_description];

          _array_description[num_description] = {..._array_description[num_description], html: html}//ajout de l'html dans le tableau descriptions
          //
          // if(num_description == 0)//on enregistre toujours la 1ère description en mémoire
          //   this.props.setHtmlDescription(html)

          this.setState({
              array_description: _array_description
          })
        }
      })
    }

    // //A chaque fois, on va charger l'description suivant l'description en cours
    // if(num_description === index_description_encours && num_description < this.state.array_description.length - 1)
    //   this.getDescription(index_description_encours + 1)
    //
    // //Si on arrive au bout des descriptions affichées, alors on charge les urls suivantes (page + 1)
    // if(index_description_encours === this.state.array_description.length - 1)
    // {
    //   this.page++
    //   this.getUrlsDescription(this.page)
    // }
  }

  handleLinks = (event) => {
    this.webview.stopLoading();
    Linking.openURL(event.url);
  }
  displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  displayPageControl = () => {
    if (!this.state.isLoading) {
      return (
        <PageControl
          style={styles.pagecontrol_container}
          numberOfPages={this.state.numberOfPages}
          currentPage={this.state.currentPage}
          hidesForSinglePage
          pageIndicatorTintColor='white'
          currentPageIndicatorTintColor='rgb(155,202,65)'
          indicatorStyle={{borderRadius: 0}}
          currentIndicatorStyle={{borderRadius: 0}}
          indicatorSize={{width:8, height:8}}
          onPageIndicatorPress={this.onItemTap}
        />
      )
    }
  }

  displayPagerDescription = () => {
    if (!this.state.isLoading) {
      if(Platform.OS === 'ios'){
        return (
          <Pages
            style={[styles.content_container, {backgroundColor: this.state.color, width: this.screenWidth}]}
            indicatorColor='#000'
            indicatorPosition='none'
            startPage={0}
            onScrollEnd={(index) => {this.changePage(index)}}>
            { this.displayDescription() }
          </Pages>
        )
      }else{
        return (
          <ViewPager
            style={[styles.content_container, {backgroundColor: this.state.color, width: this.screenWidth}]}
            initialPage={0}
            onPageSelected={(e) => {this.changePage(e.nativeEvent.position)}}
          >
            {this.displayDescription()}
          </ViewPager>
        )
      }
    }
  }

  displayDescription = () => {
    // console.tron.log({fun: 'displayDescription', state: this.state.array_description})
    return this.state.array_description.map((description, i) => {
      if(!description.html){
        return (
          <View key={i+1} style={styles.loading_container}>
            <ActivityIndicator size='large' />
          </View>
        )
      }else{
        console.tron.log({fun: 'displayDescription', html: !!description.html})
        if(Platform.OS === 'ios'){
          return (
            <WebView originWhitelist={['*']} style={styles.descriptionWebbox} source={{html: description.html}} />
          )
        }else{
          return (
            <View key={i+1}>
              <WebView originWhitelist={['*']} style={styles.descriptionWebbox} source={{html: description.html}} />
            </View>
          )
        }
      }
    })
  }

  changePage = (new_page) => {
    if (this.state.currentPage !== new_page) {
      this.setState({currentPage: new_page});
      this.getDescription(new_page)//à chaque changemet du pager, on va chercher l'html de l'description en cours d'affichage
    }
  }

  render () {
    return (
      <View style={styles.main_container}>
          {this.displayLoading()}
          {this.displayPageControl()}
          {this.displayPagerDescription()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    html_description : state.html.html_description
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setHtmlDescription : (html)=> {dispatch(HtmlActions.setHtmlDescription(html))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DescriptionScreen)
