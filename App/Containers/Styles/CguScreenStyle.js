import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles,Colors,Fonts } from '../../Themes/'


export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex:1
  },
  title: {
    marginTop: Metrics.marginVertical,
    color: Colors.charcoal,
    fontSize: 20
  },
  cgubox: {
    marginTop:Metrics.marginVertical,
    margin:10,
    padding: 10,
    borderColor:Colors.charcoal,
    backgroundColor:Colors.white

  },
  cguWebbox: {
    flex:1,
    backgroundColor:Colors.transparent,
  },
  centered: {
    alignItems: 'center'
  },
  horizontalflow:{
    flexDirection:"row",
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  label_accepted : {
    ...Fonts.style.h5,
    paddingVertical: (Metrics.baseMargin + Metrics.smallMargin),
    paddingHorizontal: Metrics.baseMargin,
    textAlign:'center',
    color: Colors.success
  },
})
