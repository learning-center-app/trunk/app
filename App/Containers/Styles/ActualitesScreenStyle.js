import { StyleSheet } from 'react-native'
import {  Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  main_container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  loading_container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent
  },

  content_container: {
    flex: 1
  },

  pagecontrol_container: {
    marginTop: 5,
    marginBottom: 10,
  },
  actuWebbox: {
    backgroundColor:Colors.transparent,
    /*marginBottom: 25*/
  },
  centered: {
    flex: 1,
    alignItems: 'center'
  },
  horizontalflow:{
    flexDirection:"row",
  },
  list: {
    flex: 1
  }
})
