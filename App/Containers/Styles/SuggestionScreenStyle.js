import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.components,
  ...ApplicationStyles.forms,
  fullWidth:{
    width:"100%",
    paddingLeft:20,
    paddingTop:8,
    paddingBottom:8
  },
  whiteBg:{
    backgroundColor:'#FFFFFF'
  },
  multilineText:{
     backgroundColor: '#000000',
     padding:20,
     minHeight:120
  },

  container:{
    flex:1
  }
})
