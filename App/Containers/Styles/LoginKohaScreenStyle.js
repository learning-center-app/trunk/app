import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.forms,

  webview: {
    flex:1,
    backgroundColor:Colors.transparent,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  image_carte : {
    resizeMode: 'contain',
    width:'100%',
    height:120,
    marginVertical: Metrics.doubleBaseMargin,
    flex:1,
  },
})
