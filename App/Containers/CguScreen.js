//@flow
import React, { Component } from 'react'
import { Platform, StatusBar, Text, View } from 'react-native'
import { WebView } from "react-native-webview"
import { connect } from 'react-redux'
import { getStatusBarHeight } from 'react-native-status-bar-height';

import SemanticButton from '../Components/SemanticButton'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import Modal from '../Components/Modal'

// Styles
import styles from './Styles/CguScreenStyle'

//Internationalisation
import { translate } from '../I18n'

class CguScreen extends Component {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};

    return {
      title: params.title || translate('SCREEN.CGU.subtitle'),
      //https://stackoverflow.com/a/49693552/1437016
      gesturesEnabled: false,
      //https://github.com/wix/react-native-navigation/blob/master/docs/screen-api.md
      // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
      // titleImage: Images.screen_icons[navigation.state.params.icon],

      //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095
    }

  }


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }
  refuse = () => {
    this.props.setVersionCguAccepte(false)
    this.setState({
      show_modal : true
    })
  }
  accept = () => {
    this.props.setVersionCguAccepte(this.props.version_cgu);
    this.props.navigation.goBack();
  }

  hideModal = () => {
    this.setState({
      show_modal : false
    })
  }
  MODAL_BUTTONS = [
    {
      text : translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        this.setState({
          show_modal : false
        })
      }
    }
  ]

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, {paddingTop: getStatusBarHeight(true)}]}><Text style={styles.sub_nav_text}>{translate('SCREEN.CGU.subtitle')}</Text></View>
        <WebView
          originWhitelist={['*']}
          source={{html:this.props.html_cgu}}
          style={styles.cguWebbox}
        />


        {
          (!!this.props.version_cgu_accepte && (this.props.version_cgu_accepte == this.props.version_cgu)) && (
            <View style={[styles.actions]}>
              <Text style={[styles.label_accepted]}>{translate('SCREEN.CGU.acceptees')}</Text>

              <SemanticButton
                text={translate('COMMON.retour')}
                type="success"
                onPress={()=> this.props.navigation.goBack()}
              />
            </View>
          )
        }
        {
          (!this.props.version_cgu_accepte || (this.props.version_cgu_accepte != this.props.version_cgu)) && (
            <View style={[styles.actions]}>
              <SemanticButton
                text={translate('COMMON.refuser')}
                type="error"
                onPress={this.refuse}
              />
              <SemanticButton
                text={translate('COMMON.accepter')}
                type="success"
                onPress={this.accept}
              />

            </View>
          )
        }

        <Modal
          isVisible={this.state.show_modal}
          title={translate('SCREEN.CGU.acceptation_obligatoire')}
          onCancel={this.hideModal}
          buttons={this.MODAL_BUTTONS}
          >
          </Modal>
      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    html_cgu : state.html.html_cgu,
    version_cgu : state.html.version_cgu,
    version_cgu_accepte: state.utilisateur.version_cgu_accepte
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CguScreen)
