//@flow
import React, { Component } from 'react'
import { Image, ScrollView, Text, TextInput, View, ActivityIndicator, TouchableOpacity } from 'react-native'
import Modal from '../Components/Modal';
import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

import { connect } from 'react-redux'
import QRCodeScanner from 'react-native-qrcode-scanner'

//Internationalisation
import { translate } from '../I18n'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import EmpruntLivreActions from '../Redux/EmpruntLivreRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// Styles
import styles from './Styles/EmpruntLivreScreenStyle'

import { Images, Fonts, Colors } from '../Themes'

class EmpruntLivreScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,

    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095

    headerRight: <TouchableOpacity onPress={() => {navigation.state.params.showModalLoginKoha()}}>
                  <Image source={Images.actions_icons.light.menu} style={[styles.menu_icon]}></Image>
                </TouchableOpacity>,

  })
  static localStyle = ({navigation}) => ({
    color: navigation.state.params.title,
  })

  constructor(props){
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
  }

  componentDidMount(){
    this.props.navigation.setParams({ showModalLoginKoha: this.showModalLoginKoha });
    this.props.navigation.setParams({ reset: ()=> this.props.reset() });

    //Ajout d'un booléen de state qui check si l'écran est affiché : si on charge la caméra dans l'écran de scan de badge, ça freeze la caméra de cet écran
    // -> focusedScreen utilise les events de la navigation pour forcer un re-render quand on revient sur l'écran
    const { navigation } = this.props;
    navigation.addListener('willFocus', () =>
      this.setState({ focusedScreen: true })
    );
    navigation.addListener('willBlur', () =>
      this.setState({ focusedScreen: false })
    );
  }
  showModalLoginKoha = () => {
    this.props.navigation.navigate("LoginKohaScreen", {color : this.state.color});
  }

  state = {
    modal:{
      success: false,
      erreur: false,
    },
  };

  SUCCESS_BUTTONS = [
    {
      text : "Retour",
      type : 'success',
      onPress : () => { this.hideModal('success') }
    }
  ]
  ERROR_BUTTONS = [
    {
      text : "Retour",
      type : 'warning',
      onPress : () => { this.hideModal('error') }
    }
  ]

  scanner = {}

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentWillReceiveProps(props) {
    if (props.sending) {
      this.setState({ button_label : "En cours..." })
    }
    else if(!props.sending){
      if (!this.state.modal.error && props.error) {
        this.setState({ modal : {error : true}})
      }
      else if (!this.state.modal.success && props.last_book) {
        this.setState({ modal : {success : true}})
      }
    }
  }

  postEmpruntLivre = (barcode) => {
    // console.tron.log(this.state.data)
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    //Vérification qu'il y a bien un identifiatn KOHA pour réserver les bouquins
    if (!this.props.utilisateur.id_koha) {
      this.props.navigation.navigate("LoginKohaScreen", {color : this.state.color});
    }
    let data = {
      barcode: barcode,
      utilisateur: {...this.props.utilisateur}
    }

    this.props.postEmpruntLivre(data);
  }

  showModal = (modal_name) => {
    let modal = {};
    modal[modal_name] = true;
    //On attend que le modal soit caché avant de mettre à jour le state redux
    this.setState({modal}, ()=>{
      //Reset du state redux, qui est persitant
      //Après un timeout de 1 seconde, le temps que le modal soit caché
      setTimeout(this.props.resetEmpruntLivre, 1000)
    });
  }

  hideModal = (modal_name) => {
    let modal = {};
    modal[modal_name] = false;
    //On attend que le modal soit caché avant de mettre à jour le state redux
    this.setState({modal}, ()=>{
      //Réactivation du scanner via methode sur référence du scanner
      //https://www.npmjs.com/package/react-native-qrcode-scanner#reactivate
      this.scanner && this.scanner.reactivate();
      //Reset du state redux, qui est persitant
      //Après un timeout de 1 seconde, le temps que le modal soit caché
      setTimeout(this.props.resetEmpruntLivre, 1000)
    });
  }

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('SCREEN.EMPRUNT_LIVRE.subtitle')}</Text></View>
        <ScrollView style={[styles.container, {backgroundColor: this.state.color}]}>
          { !!this.props.sending && <ActivityIndicator size="large" color="#FFFFFF" animating={this.props.sending} style={styles.activity_indicator}/>}
          <BetterKeyboardAvoidingView>
            { !!this.state.focusedScreen && (
                <QRCodeScanner 
                  onRead={(event)=>this.postEmpruntLivre(event.data)} 
                  ref={(node) => { this.scanner = node }} 
                  cameraProps={{ captureAudio: false }}
                  cameraStyle={{ height: 350, overflow: 'hidden' }} //https://github.com/moaazsidat/react-native-qrcode-scanner/issues/173#issuecomment-506326477
                  showMarker={true} />
            )}
            <View>
              <Text style={[styles.label, styles.padded, styles.align_center]}>
                {translate('SCREEN.EMPRUNT_LIVRE.input_code')}
              </Text>
            </View>
            <View>
              <TextInput
              style={[styles.input, styles.padded, styles.textInput]}
              placeholder="ex: 0523489"
              //  keyboardType='number-pad'
              placeholderTextColor = {Colors.slightlyTransparentText}
              enablesReturnKeyAutomatically={false}
              onChangeText={(value)=>this.setState({'barcode':value})}
              onBlur={()=>this.postEmpruntLivre(this.state.barcode)}
              returnKeyType='search'
              />
            </View>
          </BetterKeyboardAvoidingView>

        </ScrollView>
        <Modal
          isVisible={this.state.modal.success}
          onCancel={()=>this.hideModal('success')}
          title={translate('MODAL.EMPRUNT.SUCCESS.title')}
          buttons={this.SUCCESS_BUTTONS}>
          <View>
            <Text style={[ Fonts.style.h1, styles.centerText ]} >
              {!!this.props.last_book && this.props.last_book.book_title}
            </Text>
            <Text style={[ Fonts.style.h4, styles.centerText ]} >
              {!!this.props.last_book && this.props.last_book.book_authors}
            </Text>
            <Text style={[ Fonts.style.h3, styles.centerText, {paddingTop:30, paddingBottom:30} ]}>
              {!!this.props.last_book && `${translate('MODAL.EMPRUNT.SUCCESS.body')} ${this.props.last_book.book_return_date}`}
            </Text>
          </View>
        </Modal>
        {/* / MODAL ERREUR */}
        <Modal
          isVisible={this.state.modal.error}
          onCancel={()=>this.hideModal('error')}
          title={this.props.error ? translate(`ERROR_MAP.${this.props.error.error_code}.title`) : translate('ERROR.erreur')}
          buttons={this.ERROR_BUTTONS}>
          <View>
            <Text style={[ Fonts.style.h3, styles.centerText, {paddingTop:30, paddingBottom:30} ]}>
              {this.props.error ? translate(`ERROR_MAP.${this.props.error.error_code}.body`) : translate('ERROR.indisponible')}
            </Text>
          </View>
        </Modal>
      </View>
    )
  }
}

EmpruntLivreScreen.defaultProps = {
  sending : {},
  error : {},
  last_book : {},
}
const mapStateToProps = (state) => {
  return {
    sending : state.emprunt_livre.sending,
    error : state.emprunt_livre.error,
    last_book : state.emprunt_livre.last_book,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    postEmpruntLivre : data => {dispatch(EmpruntLivreActions.empruntLivreRequest(data))},
    resetEmpruntLivre : () => {dispatch(EmpruntLivreActions.empruntLivreReset())},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmpruntLivreScreen)
