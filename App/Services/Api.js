//Pour récupération numéro de version
import DeviceInfo from 'react-native-device-info'

//Internationalisation
import i18n from 'i18n-js'
// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import Secrets from 'react-native-config'

const _baseURL = Secrets.API_URL || 'http://localhost:3000'

const create = (baseURL = _baseURL) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'X-LCA-app-version': DeviceInfo.getVersion(),
      'Accept-Language': i18n.locale
    },
    // 10 second timeout...
    timeout: 10000
  })

  const getHtml = (url) => api.get(url)

  const getSalles = () => api.get('api/grr/salles')
  const getSallesReservations = () => api.get('api/grr/salles/reservations')
  const getSallesReservationsUtilisateur = (id_utilisateur) => api.get('api/grr/salles/reservations/utilisateur', id_utilisateur)

  const getMateriel = () => api.get('api/grr/materiel')
  const getMaterielReservations = () => api.get('api/grr/materiel/reservations')
  const getMaterielReservationsUtilisateur = (id_utilisateur) => api.get('api/grr/materiel/reservations/utilisateur', id_utilisateur)

  const putReservation = (data) => api.put('api/grr/reservation', data)
  const postReservation = (data) => api.post('api/grr/reservation', data)
  const deleteReservation = (data) => api.delete('api/grr/reservation', data)


  const getCGU = () => api.get('api/prismic/cgu')
  const getDescription = () => api.get('api/prismic/descriptions')
  const getActualites = (page) => {
    return api.get('api/prismic/actualites/' + page)
  }
  const getHoraires = () => api.get('api/ics/horaires')
  const getAgenda = () => api.get('api/ics/agenda')

  const postSuggestion = (data) => api.post('api/mail/suggestion', data)
  const postDemandeReference = (data) => api.post('api/mail/reference', data)
  const postRendezVous = (data) => api.post('api/mail/rdv', data)

  const postEmpruntLivre = (data) => api.post('api/koha/book', data)

  const login = () => api.get('api/shibboleth/login')

  return {
    getHtml,

    getSalles,
    getSallesReservations,

    getMateriel,
    getMaterielReservations,

    putReservation,
    postReservation,
    deleteReservation,

    getCGU,
    getDescription,
    getActualites,

    getHoraires,
    getAgenda,

    postSuggestion,
    postDemandeReference,
    postRendezVous,

    postEmpruntLivre,

    login,
  }
}

// let's return back our create method as the default.
export default {
  create,
  baseURL : _baseURL
}
