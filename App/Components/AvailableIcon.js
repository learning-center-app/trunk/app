//@flow
import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { TouchableOpacity, Image } from 'react-native'
import styles from './Styles/AvailableIconStyle'
import PropTypes from 'prop-types';

import SemanticButton from './SemanticButton'
import { Images } from '../Themes'

export default class AvailableButton extends Component {
  constructor(props) {
    super(props);
    this.state = this.getButtonTypeFromStatus(props.status)
  }

  //Mise à jour du state si les props changent
  componentWillReceiveProps(nextProps) {
    this.setState(this.getButtonTypeFromStatus(nextProps.status))
  }

  getButtonTypeFromStatus = (type) => {
    let icon_name, onPress;
    switch (type) {
      case 'conflict':
        icon_name = '';
        onPress = this.props.onConflict;
        break;
      case 'available':
        icon_name = 'add';
        onPress = this.props.onAvailable;
        break;
      case 'unavailable':
        icon_name = '';
        onPress = this.props.onUnavailable;
        break;
      case 'reserved':
        icon_name = 'edit';
        onPress = this.props.onReserved;
        break;
      default:
        icon_name = 'info';
        onPress = this.props.onUnavailable;
        break;
    }
    //S'il n'y a pas de fonction handler
    if (typeof onPress != "function") {
      //On n'affiche pas de bouton, pas la peine
      icon_name = '';
    }
    return {icon_name, onPress};
  }
  // bgColor = {}

  static propTypes = {
    status: PropTypes.string,
    onConflict: PropTypes.func,
    onAvailable: PropTypes.func,
    onUnavailable: PropTypes.func,
    onReserved: PropTypes.func,
  }

  render () {
    return (
      <TouchableOpacity onPress={this.state.onPress} style={styles.padded}>
          <Image style={styles.icon} source={Images.actions_icons.light[this.state.icon_name]}/>
      </TouchableOpacity>
    )
  }
}
