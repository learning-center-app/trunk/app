//@flow
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity, Animated } from 'react-native'
import HTMLView from './HTMLView';

import styles from './Styles/ReservationItemStyle'
import { Images, Fonts, Colors } from '../Themes'
import CollapsableItem from './CollapsableItem'
import SemanticButton from './SemanticButton'
import AvailableIcon from './AvailableIcon'

import moment from 'moment'
import 'moment/locale/fr'

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'


export default class ReservationItem extends Component {
  constructor(props) {
    super(props);
    //Définition de la langue pour MomentJS : http://momentjs.com/docs/#/i18n/loading-into-nodejs/
    console.tron.log(i18n.locale)
    moment.locale(i18n.locale)
  }
  getRelativeDisplay = (item) => {
    var now = moment();
      start = moment.unix(item.start_time),
      end = moment.unix(item.end_time);

    //Si la résa est déjà commencée, et pas finie
    if (now.isBetween(start, end))
      return `${translate('COMMON.fini')} ${moment.unix(this.props.item.end_time).fromNow()}`
    //Si la résa n'est pas encore commencée
    else if (now.isBefore(start))
      return `${translate('COMMON.commence')} ${moment.unix(this.props.item.start_time).fromNow()}`
    //Si la résa est passée
    else
      return moment.unix(this.props.item.end_time).fromNow()
  }
  getDateDisplay = (item) => {
    var start = moment.unix(item.start_time),
          end = moment.unix(item.end_time);

    //Si c'est le même jour
    if (start.isSame(end, 'day'))
      return `${start.format('dddd LL')} ${translate('COMMON.de')} ${start.format('H:mm')} ${translate('COMMON.a')} ${end.format('H:mm')}`
    else
      return `${translate('COMMON.du')} ${start.format('dddd LL')} ${start.format('H:mm')}\n${translate('COMMON.au')} ${end.format('dddd LL')} ${end.format('H:mm')}`
  }

  // // Prop type warnings
  static propTypes = {
    item: PropTypes.object.isRequired,
    types_reservation: PropTypes.object.isRequired,
    handlers: PropTypes.object,
    backgroundColor: PropTypes.string,
  }

  // // Defaults for props
  static defaultProps = {
    item: {}
  }

  render () {
    return (
      <CollapsableItem backgroundColor={this.props.backgroundColor}>
        <View style={[styles.row, styles.inline, {justifyContent:'flex-start'}]}>
          {
            //Cast en booléen pour éviter erreur : https://github.com/facebook/react-native/issues/18773#issuecomment-380237027
            !!this.props.item.image && <Image source={{uri: this.props.item.image}} style={styles.illustration} />
          }
          <View style={[styles.rowContent, styles.inline_no_wrap]}>
            <View style={{flex:1}}>
              <Text
                adjustsFontSizeToFit
                numberOfLines={1}
                style={styles.list_item_title}>{this.props.item.salle || this.props.item.materiel}</Text>
              {/* <Text style={Fonts.style.h5}>{this.props.item.image}</Text> */}
              <Text style={Fonts.style.h5}>{this.getRelativeDisplay(this.props.item)}</Text>
            </View>

            <AvailableIcon
              status='reserved'
              onAvailable={()=>{this.props.handlers.onAvailable(this.props.item)}}
              onConflict={()=>{this.props.handlers.onConflict(this.props.item)}}
              onReserved={()=>{this.props.handlers.onReserved(this.props.item)}}
              onUnavailable={()=>{this.props.handlers.onUnavailable(this.props.item)}}></AvailableIcon>
          </View>
        </View>
        <View>
          <Text style={[styles.label, Fonts.style.h5]}>{this.getDateDisplay(this.props.item)}</Text>
          {/*<Text style={[styles.label, Fonts.style.h5]}>{this.props.types_reservation[this.props.item.type]}</Text>*/}
          { !!this.props.item.campus && <Text style={[styles.label, Fonts.style.h5]}>{this.props.item.campus}</Text>}
          { !!this.props.item.description && <Text style={styles.label}>{this.props.item.description}</Text>}
          { !!this.props.item.comment_room && <HTMLView style={styles.html} value={this.props.item.comment_room}></HTMLView>}
        </View>
      </CollapsableItem>

    )
  }
}
