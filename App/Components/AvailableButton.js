import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/AvailableButtonStyle'
import PropTypes from 'prop-types';

import SemanticButton from './SemanticButton'

//Internationalisation
import { translate } from '../I18n'

export default class AvailableButton extends PureComponent {
  constructor(props) {
    super(props);
    this.state = this.getButtonTypeFromStatus(props.status)
  }

  //Mise à jour du state si les props changent
  componentWillReceiveProps(nextProps) {
    this.setState(this.getButtonTypeFromStatus(nextProps.status))
  }
  
  getButtonTypeFromStatus = (type) => {
    let button_type, button_text, onPress;
    switch (type) {
      case 'conflict':
        button_type = 'warning';
        button_text = translate('COMPONENT.AVAILABILTY.conflit');
        onPress = this.props.onConflict;
        break;
      case 'available':
        button_type = 'success';
        button_text = translate('COMPONENT.AVAILABILTY.disponible');
        onPress = this.props.onAvailable;
        break;
      case 'unavailable':
        button_type = 'error';
        button_text = translate('COMPONENT.AVAILABILTY.indisponible');
        onPress = this.props.onUnavailable;
        break;
      default:
        button_type = 'info';
        button_text = translate('COMPONENT.AVAILABILTY.inconnu');
        onPress = this.props.onUnavailable;
        break;
    }
    return {button_type, button_text, onPress};
  }
  // bgColor = {}

  static propTypes = {
    status: PropTypes.string,
    onPress: PropTypes.func
  }

  render () {
    return (
      <SemanticButton text={this.state.button_text} type={this.state.button_type} onPress={this.state.onPress}>
      </SemanticButton>
    )
  }
}
