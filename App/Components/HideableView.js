//@flow
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Animated } from 'react-native'
import styles from './Styles/HideableViewStyle'

const MIN_HEIGHT = 0;
const MAX_HEIGHT = 100;

export default class HideableView extends Component {
  constructor(props) {
    super(props);

  }

  // // Prop type warnings
  static propTypes = {
    isVisible: PropTypes.bool,
    minHeight: PropTypes.number,
    maxHeight: PropTypes.number
  }
  state = {
    expanded: false,
    animation: new Animated.Value(MIN_HEIGHT),
    minHeight: MIN_HEIGHT,
    maxHeight: MAX_HEIGHT,
  };

  componentDidMount() {
    this.setState({
      expanded: this.props.isVisible,
      animation: new Animated.Value(MIN_HEIGHT),
      minHeight: this.props.minHeight || MIN_HEIGHT,
      maxHeight: this.props.maxHeight || MAX_HEIGHT,
    })
    if (this.props.isVisible == true) {
      toggle();
    }
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    // console.tron.log(this.props.sorted_by_campus)
    // console.tron.log(prevProps.sorted_by_campus !== this.props.sorted_by_campus)
    if (prevProps.isVisible !== this.props.isVisible) {
      this.toggle()
    }
  }
  _setMaxHeight(event){
    this.setState({
        maxHeight   : event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event){
    this.setState({
        minHeight   : event.nativeEvent.layout.height
    });
  }
  toggle(){
    console.tron.log(this.state)
    //Step 1
    let initialValue    = this.state.expanded? this.state.maxHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight;

    this.setState({
        expanded : !this.state.expanded  //Step 2
    });

    this.state.animation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start();  //Step 5
  }

  render () {
    return (
      <Animated.View style={[styles.container, {height: this.state.animation}]}>
        {this.props.children}
      </Animated.View>

    )
  }
}
