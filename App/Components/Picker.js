import React, { Component } from 'react'
import { View } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { ApplicationStyles } from '../Themes'

export default class Picker extends Component {

  getText () {
    const buttonText = this.props.text || this.props.children || ''
    return buttonText.toUpperCase()
  }

  render () {
    return (
      <RNPickerSelect
        hideDoneBar={true}
        style={ApplicationStyles.picker}
        useNativeAndroidPickerStyle={false}
        Icon={() => {
          return (
            <View
              style={ApplicationStyles.picker_icon}
            />
          );
        }}
        {...this.props}
      />
    )
  }
}
