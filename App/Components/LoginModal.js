//@flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/LoginModalStyle'
import Modal from './Modal';

import UtilisateurActions from '../Redux/UtilisateurRedux'
import SemanticButton from './SemanticButton'

class LoginModal extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   isVisible: PropTypes.bool.isRequired,
  // }
  //
  // Defaults for props
  static defaultProps = {
    show_login: false
  }

  // state = {
  //   show_login : false
  // }

  //Options pour le modal
  //Définitions des boutons
  LOGIN_BUTTONS = [
    {
      text : 'Annuler',
      type : 'warning',
      onPress : () => {
        this.props.utilisateurShowLogin()
      }
    },
    {
      text : 'Connexion',
      type : 'success',
      onPress : () => {
        console.tron.log("Requete de connexion")
        // this.props.utilisateurShowLogin()
        this.props.utilisateurRequest()
      }
    }
  ]
  render () {
    return (
      <View>
        <Modal
          isVisible={this.props.show_login}
          title="Vous devez être connecté pour continuer."
          onCancel={() => this.props.utilisateurShowLogin()}
          buttons={this.LOGIN_BUTTONS}>
        </Modal>
      </View>
    )
  }
}

LoginModal.defaultProps = {
  show_login: false
}
const mapStateToProps = (state) => {
  return {
    show_login: state.utilisateur.show_login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurRequest: ()=>{dispatch(UtilisateurActions.utilisateurRequest())},
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal)
