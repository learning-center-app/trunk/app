import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  modal_container: {
    // flex: 1,
    backgroundColor: Colors.background,
    padding:20
  },
  modal_title: {
    fontSize: Fonts.size.h4,
    padding:20
  },
  modal_actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

})
