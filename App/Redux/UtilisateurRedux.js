//@flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Reactotron from 'reactotron-react-native'
import Analytics from 'appcenter-analytics';

import TransformArrayToValuesForSelect from '../Transforms/TransformArrayToValuesForSelect'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  utilisateurShowLogin: null,
  utilisateurShowInternet: null,
  utilisateurLogout: null,
  utilisateurRequest: ['data'],
  utilisateurSuccess: ['payload'],
  utilisateurFailure: null,
  setUtilisateurIdKoha: ['id_koha'],
  setUtilisateurCampus: ['campus'],
  setTutorielVu: ['vu'],
  setVersionCguAccepte: ['version_cgu_accepte'],
  setLangue: ['langue'],
})

export const UtilisateurTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userId: null,  //'mjugan12'
  email: null,  //'maen.juganaikloo@imt-atlantique.fr'
  eppn: null,  //'mjugan12@emn.fr'
  name: null,  //'Maen Juganaikloo'
  organization: null,  //'Pairform'
  id_koha: null,
  campus: null,
  formation: null,
  service: null,

  version_cgu_accepte: null,

  data: null,
  fetchingUser: null,
  payload: null,
  error: null,
  tutoriel_est_vu: null,
  langue: null,

  show_login : null,
  show_internet : null,
})

//Note: les noms ne sont plus utilisés, seuls les codes le sont pour i18n
export const LIST_CAMPUS = Immutable([
  {
    id: 1,
    code: 'NA',
  },
  {
    id: 2,
    code: 'BR',
  },
  {
    id: 3,
    code: 'RE',
  },
  {
    id: 4,
    code: 'TO',
  },
])

/* ------------- Selectors ------------- */

export const UtilisateurSelectors = {
  getCguVersion: state => state.version_cgu_accepte,
  isUtilisateurLoggedIn: state => state.userId,
  getLangue: state => state.langue,
  //Deprecated
  getListCampusForSelect: () => TransformArrayToValuesForSelect(LIST_CAMPUS, 'id', 'name')
}

/* ------------- Reducers ------------- */
// Login de l'utilisateur
export const login = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// Récupération OK des infos utilisateur
//show_login = false pour cacher le popup de connexion
export const success = (state, action) => {
  Analytics.trackEvent('[Utilisateur] Connexion réussie');
  const { payload } = action
  return state.merge({ fetching: false, show_login: false, error: null, ...payload })
}

//Logout de l'utilsateur connect&
export const logout = (state, action) => {
  Analytics.trackEvent('[Utilisateur] Déconnexion');
  return state.merge({ userId: null, email: null, eppn: null, name: null, organization: null, id_koha: null})
}

// Something went wrong somewhere.
export const failure = state =>{
  Analytics.trackEvent('[Utilisateur] Login annulé');
  return state.merge({ fetching: false, error: true, payload: null })
}

// Set l'identifiant KOHA
export const setIdKoha = (state, data) => {
  Analytics.trackEvent('[Utilisateur] Identifiant koha renseigné');
  return state.merge({ id_koha: data.id_koha })
}

// Set le campus actuel
export const setCampus = (state, data) => {
  Analytics.trackEvent('[Utilisateur] Campus renseigné', data.campus);
  return state.merge({ campus: data.campus })
}

// Set le flag de tutoriel
export const setTutorielVu = (state, data) => {
  console.log(data.vu)
  return state.merge({ tutoriel_est_vu: data.vu })
}

// Set un flag pour avertir le RootContainer qu'il doit afficher un modal de login
export const toggleLogin = (state, data) => {
  ///!!! : convertit en booléen
  //null|undefined devient true puis devient false puis devient true
  //true|"haha"|{qqchose:"truc"} devient false puis devient true puis devient false
  return state.merge({show_login : !!!state.show_login })
}


// Set un flag pour avertir le RootContainer qu'il doit afficher un modal de login
export const toggleInternet = (state, data) => {
  console.tron.log(state)
  ///!!! : convertit en booléen
  //null|undefined devient true puis devient false puis devient true
  //true|"haha"|{qqchose:"truc"} devient false puis devient true puis devient false
  return state.merge({show_internet : !!!state.show_internet })
}

// Set un flag pour avertir le RootContainer qu'il doit afficher un modal de login
export const setVersionCguAccepte = (state, data) => {
  console.tron.log({data_cgu_redux: data})
  return state.merge({version_cgu_accepte : data.version_cgu_accepte})
}

/** Force la langue de l'affichage
 * @param {*} data.langue - ex: { languageTag: "en", isRTL: false }
 */
export const setLangue = (state, data) => {
  return state.merge({ langue: data.langue })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UTILISATEUR_REQUEST]: login,
  [Types.UTILISATEUR_SUCCESS]: success,
  [Types.UTILISATEUR_FAILURE]: failure,
  [Types.SET_UTILISATEUR_ID_KOHA]: setIdKoha,
  [Types.SET_UTILISATEUR_CAMPUS]: setCampus,
  [Types.SET_TUTORIEL_VU]: setTutorielVu,
  [Types.SET_VERSION_CGU_ACCEPTE]: setVersionCguAccepte,
  [Types.SET_LANGUE]: setLangue,
  [Types.UTILISATEUR_LOGOUT]: logout,
  [Types.UTILISATEUR_SHOW_LOGIN]: toggleLogin,
  [Types.UTILISATEUR_SHOW_INTERNET]: toggleInternet,
})
