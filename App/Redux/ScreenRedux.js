import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  updateScreenDescription: ['list', 'description']
})

export const ScreenTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  list: [
      {
        icon: 'dispo_salle',
        color: 'rgb(0, 195, 194)',
        title : "Réservation salle 1 heure",
        description : "Description",
        needs_login: false,
        screen : "DispoSalleScreen"
      },
      {
        icon: 'reservation_salle',
        color: 'rgb(0, 171, 230)',
        title : "Réservation de salle",
        description : "Description",
        needs_login: true,
        screen : "MesReservationsSalleScreen"
      },
      {
        icon: 'reservation_materiel',
        color: 'rgb(53, 125, 199)',
        title : "Réservation de matériel",
        description : "Description",
        needs_login: true,
        screen : "MesReservationsMaterielScreen"
      },
      {
        icon: 'emprunt_livre',
        color: 'rgb(255, 104, 80)',
        title : "Prêt de livre",
        description : "Description",
        needs_login: true,
        screen : "EmpruntLivreScreen"
      },
      {
        icon: 'demande_reference',
        color: 'rgb(245, 161, 101)',
        title: "Demande de réference",
        description: "Description",
        needs_login: true,
        screen: "DemandeReferenceScreen"
      },
      {
        icon: 'suggestion',
        color: 'rgb(0, 187, 224)',
        title : "Suggestion",
        description : "Description",
        needs_login: true,
        screen : "SuggestionScreen"
      },
      {
        icon: 'rendez_vous',
        color: '#a55c5d',
        title : "Rendez-vous",
        description : "Description",
        needs_login: true,
        screen : "RendezVousScreen"
      },
      {
        icon: 'agenda',
        color: 'rgb(113, 174, 36)',
        title : "Agenda",
        description : "Description",
        needs_login: false,
        screen : "AgendaScreen"
      },
      {
        icon: 'actualites',
        color: 'rgb(158, 192, 224)',
        title : "Actualités",
        description : "Description",
        needs_login: false,
        screen : "ActualitesScreen"
      },
      {
        icon: 'horaires',
        color: 'rgb(0, 132, 104)',
        title : "Horaires",
        description : "Description",
        needs_login: false,
        screen : "HorairesScreen"
      },
      {
        icon: 'description',
        color: 'rgb(110,126,152)',
        title: "Description",
        description: "Description",
        needs_login: false,
        screen: "DescriptionScreen"
      },
    ],
    menu : [
      {
        color: 'rgb(0,0,0)',
        title : "Tutoriel",
        description : "Conditions générales",
        needs_login: false,
        screen : "LaunchScreen"
      },
      {
        color: 'rgb(0,0,0)',
        title : "CGU",
        description : "Conditions générales d'utilisation",
        needs_login: false,
        screen : "CguScreen"
      },
      {
        color: 'rgb(0,0,0)',
        title : "Mentions légales",
        description : "Mentions légales",
        needs_login: false,
        screen : "MentionsLegalesScreen"
      },
      // {
      //   color: 'rgb(0,0,0)',
      //   title : "Signaler un bug",
      //   description : "Signaler un bug",
      //   needs_login: false
      // },
    ]
}

/* ------------- Selectors ------------- */

export const ScreenSelectors = {
  // selectAvatar: state => state.github.avatar
}

/* ------------- Reducers ------------- */

// successful avatar lookup
export const updateScreenDescription = (state, action) => {
  const { screen, description } = action;
  console.log(action);
  console.log(state);
  screen.description = description;
  debugger
  return state.merge({ list: [screen]})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_SCREEN_DESCRIPTION]: updateScreenDescription
})
