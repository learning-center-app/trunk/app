import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

import UpdateReservationsInSalles from '../Transforms/UpdateReservationsInSalles'
import AddReservationToSalle from '../Transforms/AddReservationToSalle'
import FilterReservationsByUser from '../Transforms/FilterReservationsByUser'
import SortReservationsBySalles from '../Transforms/SortReservationsBySalles'
import OrderResaByStartDate from '../Transforms/OrderResaByStartDate'
import SortSallesByCampus from '../Transforms/SortSallesByCampus'
import SortSectionnedListByUtilisateurCampus from '../Transforms/SortSectionnedListByUtilisateurCampus'
import TransformKeysToValuesForSelect from '../Transforms/TransformKeysToValuesForSelect'
import EditReservation from '../Transforms/EditReservation'
import DeleteReservation from '../Transforms/DeleteReservation'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sallesRequest: null,
  sallesSuccess: ['raw'],
  sallesFailure: null,

  sallesReservationsRequest: null,
  sallesReservationsSuccess: ['raw'],
  sallesReservationsFailure: null,

  putSalleReservationRequest : ['data'],
  putSalleReservationSuccess : ['reservation'],
  putSalleReservationFailure : null,
  putSalleReservationSoftFailure : ['error'],

  postSalleReservationRequest : ['data'],
  postSalleReservationSuccess : ['reservation'],
  postSalleReservationFailure : null,

  deleteSalleReservationRequest : ['data'],
  deleteSalleReservationSuccess : ['reservation'],
  deleteSalleReservationFailure : ['error'],
})

export const SallesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  raw: null,
  reservations : {
    raw : [],
    fetching : null,
    sending : null,
    sending_error : null,
    lastReservation : null
  },
  configuration : {
    types_reservation : {
      'A': 'Enseignement',
      'B': 'Réunion',
      'C': 'Projet',
      'D': 'Gouvernance',
      'E': 'Recherche',
      'F': 'Service',
      'G': 'Autre'
    }
  },
  error: null
})

/* ------------- Selectors ------------- */

export const SallesSelectors = {
  getSallesSortedByCampus : state => {
    // console.tron.log({'state.salles.reservations.raw.length' : state.salles.reservations.raw.length})
    let _resas = SortReservationsBySalles(state.salles.reservations.raw);
    // console.tron.log({_resas : _resas})
    let _salles = UpdateReservationsInSalles(_resas, state.salles.raw);
    // console.tron.log({_salles : _salles})
    let _sorted_by_campus = SortSallesByCampus(_salles);
    // console.tron.log({_sorted_by_campus : _sorted_by_campus})
    let _sorted_by_campus_and_utilisateur = SortSectionnedListByUtilisateurCampus(_sorted_by_campus, state.utilisateur.campus && state.utilisateur.campus.id);
    // console.tron.log({_sorted_by_campus_and_utilisateur : _sorted_by_campus_and_utilisateur})

    return _sorted_by_campus_and_utilisateur;
  },
  getReservationsByUser: state => {
    let reservations = OrderResaByStartDate(FilterReservationsByUser(state.salles.reservations.raw, state.utilisateur.userId));
    // console.tron.log(reservations);
    return reservations;
  },
  getTypesReservationsForSelect: state => {
    return TransformKeysToValuesForSelect(state.salles.configuration.types_reservation);
  }
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ fetching: true})

// successful api lookup
export const success = (state, action) => {
  const { raw } = action
  return state.merge({ fetching: false, error: null, raw })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true })

/* Reservations */
// request the data from an api
export const requestReservations = (state) =>
  state.merge({ fetching: true})

// successful api lookup
export const successReservations = (state, action) => {
  const { raw } = action
  return state.merge({ fetching: false, error: null, reservations : {raw}})
}

// Something went wrong somewhere.
export const failureReservations = state =>
  state.merge({ fetching: false, error: true })


/* Envoi d'une réservation */

export const requestPutSalleReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successPutSalleReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Salle] Réservation réussie', {nom: reservation.salle, campus: reservation.campus_name});
  return state.merge({
    reservations: {
      raw : state.reservations.raw ? state.reservations.raw.concat([reservation]) : [reservation],
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const softFailurePutSalleReservation = (state, action) =>{
  const { error } = action
  Analytics.trackEvent('[Salle] Réservation échouée', error);
  state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}
//Erreur non-gérée par le serveur (erreur 500)
export const failurePutSalleReservation = state =>{
  Analytics.trackEvent('[Salle] Réservation échouée');
  return state.merge({ reservations: {...state.reservations, sending: false, sending_error: true}})
}


/********* Edition d'une réservation *********/

export const requestPostSalleReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successPostSalleReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Salle] Modification d\'une réservation réussie', {nom: reservation.salle, campus: reservation.campus_name});
  return state.merge({
    reservations: {
      raw : EditReservation(state.reservations.raw, reservation),
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const failurePostSalleReservation = (state, action) =>{
  const { error } = action
  Analytics.trackEvent('[Salle] Modification d\'une réservation échouée', error);
  state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}

/********* Suppression d'une réservation *********/

export const requestDeleteSalleReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successDeleteSalleReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Salle] Suppression d\'une réservation réussie', {nom: reservation.salle, campus: reservation.campus_name});
  return state.merge({
    reservations: {
      raw : DeleteReservation(state.reservations.raw, reservation),
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const failureDeleteSalleReservation = (state, action) =>{
  const { error } = action
  Analytics.trackEvent('[Salle] Suppression d\'une réservation échouée', error);
  console.tron.log({action: action});
  return state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SALLES_REQUEST]: request,
  [Types.SALLES_SUCCESS]: success,
  [Types.SALLES_FAILURE]: failure,

  [Types.SALLES_RESERVATIONS_REQUEST]: requestReservations,
  [Types.SALLES_RESERVATIONS_SUCCESS]: successReservations,
  [Types.SALLES_RESERVATIONS_FAILURE]: failureReservations,

  [Types.PUT_SALLE_RESERVATION_REQUEST]: requestPutSalleReservation,
  [Types.PUT_SALLE_RESERVATION_SUCCESS]: successPutSalleReservation,
  [Types.PUT_SALLE_RESERVATION_FAILURE]: failurePutSalleReservation,
  [Types.PUT_SALLE_RESERVATION_SOFT_FAILURE]: softFailurePutSalleReservation,

  [Types.POST_SALLE_RESERVATION_REQUEST]: requestPostSalleReservation,
  [Types.POST_SALLE_RESERVATION_SUCCESS]: successPostSalleReservation,
  [Types.POST_SALLE_RESERVATION_FAILURE]: failurePostSalleReservation,

  [Types.DELETE_SALLE_RESERVATION_REQUEST]: requestDeleteSalleReservation,
  [Types.DELETE_SALLE_RESERVATION_SUCCESS]: successDeleteSalleReservation,
  [Types.DELETE_SALLE_RESERVATION_FAILURE]: failureDeleteSalleReservation,
})
