import Salle from '../Classes/Salle'

export default SortSallesByCampus = (salles) => {
  var data = [];

  let obj = salles.reduce((result, salle) => {
    if(typeof result[salle.area_id] == "undefined")
      result[salle.area_id] = {id:salle.area_id, campus_name:salle.campus_name, campus_id:salle.campus_id, type_materiel:salle.type_materiel, data: []}
    result[salle.area_id].data.push(new Salle(salle, salle.reservations));
    return result;
  }, {})
  for (var campus in obj) {
    if (obj.hasOwnProperty(campus)) {
      data.push(obj[campus])
    }
  }
  console.tron.log(data);
  return data;
}
