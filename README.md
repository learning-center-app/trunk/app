#  LCA

NPM : 6.5.0
Node : 8.0.0

Compatible React Native App, bootstrapé avec [Ignite](https://github.com/infinitered/ignite)

## Pour commencer

Cloner ce repository
Installer les dépendences via `yarn` or `npm i`


### Configuration des variables d'environnement 

Copier `.env.example` en `.env`, puis renseigner l'URL de l'API

```
API_URL=https://myapi.com
```


### Run & build

Pour builder :

iOS (nécessite un mac) : 
```
$ cd ios && pod install
$ react-native run-ios
```

Android : 
```
$ react-native run-android
```
Pour débugger sur un device, il faut le .jks, voir section suivante



## Pour déployer

### Android
Google Play needs a signed APK, so you'll have to create a keystore and sign up your build:
1. Copy *android/app/gradle.properties.example* to *android/app/gradle.properties*
2. Generate a **.jks** file : https://stackoverflow.com/a/44103025/1437016
3. Copy your **.jks** in *android/app*
4. Update *android/app/gradle.properties* to point to your **.jks** file and its metadata
5. Run *npm run android:build* to generate a **.aab bundle** (recommended by google for size optimization)
6. Find your **.aab** under *android/app/build/generated/outputs/bundle/release/app.aab*

### iOS
Step to check before making an archive and upload it via XCode:

1. Don't forget to remove *NSAppTransportSecurity* from *Info.plist* if you use an http localhost server. You can just change *NSAppTransportSecurity* to *DISABLED_FOR_PROD_NSAppTransportSecurity*.